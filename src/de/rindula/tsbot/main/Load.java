/*
 * Copyright (c) 2019, Sven Nolting, All Rights Reserved
 */

package de.rindula.tsbot.main;

import com.github.theholywaffle.teamspeak3.TS3ApiAsync;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.TS3Query.FloodRate;
import com.github.theholywaffle.teamspeak3.api.CommandFuture;
import com.github.theholywaffle.teamspeak3.api.VirtualServerProperty;
import com.github.theholywaffle.teamspeak3.api.reconnect.ReconnectStrategy;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.ServerGroupClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Load {

	/**
	 * Eine Liste mit offenen Supportfällen im Warteraum
	 */
	public static final ArrayList<String> openCases = new ArrayList<>();
	public static final Logger log = Logger.getLogger(Load.class.getName());
	private static final TS3Config config = new TS3Config();
	private static final TS3Query query = new TS3Query(config);
	private static final TS3ApiAsync api = new TS3ApiAsync(query);
	private static final ArrayList<Integer> sgrp = new ArrayList<>();
	private static final HashMap<String, URL> developerUIds = new HashMap<>();
	private static final Properties prop = new Properties();
	private static String host;
	private static String user;
	private static String pass;
	private static int cid;
    public static boolean finished = false;
	private static Status status;

	public static void setStatus(Status status) {
		Load.status = status;
	}

	public static TS3ApiAsync getApi() {
		return api;
	}
	/**
	 * @return TS UIds der Bot-Entwickler
	 */
	public static HashMap<String, URL> getDeveloperUIds() {
		return developerUIds;
	}

	public static void main(String[] args) {

		File f = new File("./config.xml");
		if (!f.exists() || f.isDirectory()) {
			createNewConfigFile(true);
		}
		readConfigFile();

		configureDevs();
		
		config.setHost(host);
		config.setFloodRate(FloodRate.DEFAULT);
		config.setReconnectStrategy(ReconnectStrategy.disconnect());

		log.setLevel(Level.ALL);
		try {
			log.addHandler(new FileHandler("logfile.log"));
		} catch (SecurityException | IOException e1) {
			e1.printStackTrace();
		}

		query.connect();
		api.login(user, pass);
		api.selectVirtualServerByPort(9987);
		api.moveQuery(cid);
		api.setNickname("Supportbot");
		Events.loadEvents();
		if (query.isConnected()) {
			log.info("Erfolgreich verbunden zu \"" + host + "\"");
			Runtime.getRuntime().addShutdownHook(new Thread(() -> {
				query.exit();
			}));
			ArrayList<String> sentNames = new ArrayList<>();
			try {
				for (int grp :
						sgrp) {
					for (ServerGroupClient c : api.getServerGroupClients(grp).get()) {
						final CommandFuture<List<Client>> clients = api.getClients();
						for (Client cl :
								clients.get()) {
							if (cl.getNickname().equals(c.getNickname()) && cl.isRegularClient()) {
								try {
									if (!sentNames.contains(api.getClientInfo(api.getClientByUId(c.getUniqueIdentifier()).get().getId()).get().getNickname())) {
										sendWelcomeMessage(api.getClientInfo(api.getClientByUId(c.getUniqueIdentifier()).get().getId()));
										sentNames.add(api.getClientInfo(api.getClientByUId(c.getUniqueIdentifier()).get().getId()).get().getNickname());
									}
								} catch (Exception e) {
									// Nothing to see here ...
								}
							}
						}

					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
            Utils.updateSupport();
			Timer t = new Timer();
			t.schedule(new TimerTask() {
				@Override
				public void run() {
					if (Utils.getLastUpdate() < (System.currentTimeMillis() - 10000)) {
						Utils.updateSupport();
					}
				}
			}, 1000, 5000);

			if (host.equalsIgnoreCase("rindula.de")) {
				Map<VirtualServerProperty, String> prop = new HashMap<>();
				prop.put(VirtualServerProperty.VIRTUALSERVER_NAME, "Rindulas Testserver");
				prop.put(VirtualServerProperty.VIRTUALSERVER_WELCOMEMESSAGE, "Willkommen auf dem Testserver von Rindula");

				api.editServer(prop);
			}
        } else {
			System.exit(5);
		}

	}

	/**
	 * Developer UIDs mit dem gewollten Icon verbinden ... Ich m&ouml;chte ja auffallen :D
	 */
	private static void configureDevs() {
		try {
			developerUIds.put("oZWbGh5xoIkWZb08aI2d1cmhEPQ=", new URL("https://mc-heads.net/avatar/Rindula/16.png"));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	public static void sendWelcomeMessage(CommandFuture<ClientInfo> clientInfo) {
        String message = "TS3 Support Bot Online - Made by Rindula";
		try {
			api.sendPrivateMessage(clientInfo.get().getId(), message);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return Gets the Status of the Supportchannel
	 */
	@SuppressWarnings("WeakerAccess")
	public static Status getStatus() {
		return status;
	}

	/**
	 * @return Gets the Channel ID of the Supportchannel
	 */
	@SuppressWarnings("WeakerAccess")
	public static int getCid() {
		return cid;
	}

	/**
	 * Erstellt eine neue Konfiguration auf Basis von Benutzereingaben
	 * @param stopAfterCreation Soll der Bot beendet werden, nachdem die eingabe getätigt wurde? Nützlich, um sicher zu gehen, dass alles neu geladen wird.
	 */
	private static void createNewConfigFile(boolean stopAfterCreation) {
		try {
			// set the properties value
			prop.setProperty("host", getInput("Bitte gib den TS3 Hostname ein: "));
			prop.setProperty("user", getInput("Bitte gib den TS3 Queryusername ein: "));
			prop.setProperty("password", getInput("Bitte gib das TS3 Querypasswort ein: "));
			prop.setProperty("channelid", getInput("Bitte gib die ID des \"Warte auf Support Channel\"'s ein: "));
            prop.setProperty("supportgruppe", getInput("Bitte gib die IDs der Supportgruppen (Kommagetrennt, ohne leerzeichen) ein: "));
			prop.setProperty("talkchannels", "");

			// save properties to project root folder
			prop.storeToXML(new FileOutputStream("./config.xml"), "General Settings");
			log.config("Config file created");
			log.info("You can modify the config.properties at any time, to apply the changes you have to restart the Bot.");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		if (stopAfterCreation) System.exit(0);
	}

	/**
	 * Liest die Benutzereingabe in der Konsole
	 * @param string Die Frage, welche dem Benutzer gestellt wird
	 * @return Die Benutzer eingabe
	 */
	private static String getInput(String string) {
		try {
			System.out.print(string);
			Scanner in = new Scanner(System.in);
            return in.next();
		} catch (Exception e) {
            e.printStackTrace();
			return "Error - To change!";
		}
	}


	@SuppressWarnings("WeakerAccess")
	public static ArrayList<Integer> getGroup() {
		return sgrp;
	}

	/**
	 * Liest die Configdatei neu ein
	 */
	private static void readConfigFile() {
		try {
			prop.loadFromXML(new FileInputStream("./config.xml"));
			host = (String) prop.get("host");
			user = (String) prop.get("user");
			pass = (String) prop.get("password");
			cid = Integer.valueOf((String) prop.get("channelid"));
			String[] tmp = ((String) prop.get("supportgruppe")).trim().split(",");
			for (String grp :
					tmp) {
				sgrp.add(Integer.valueOf(grp));
			}
			String[] tmpc = ((String) prop.get("talkchannels")).trim().split(",");
			for (String s :
					tmpc) {
				Utils.getgChannels().put(s, new ArrayList<>());
			}
			log.config("Config file read, variables set");
		} catch (IOException e) {
			createNewConfigFile(false);
			e.printStackTrace();
		}
	}

}
