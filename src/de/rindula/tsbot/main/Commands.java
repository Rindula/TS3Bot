package de.rindula.tsbot.main;

import com.github.theholywaffle.teamspeak3.api.ChannelProperty;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Commands {

    /**
     * Beendet den Bot
     */
    public static void exit() {
        Load.finished = true;
        Map<ChannelProperty, String> options = new HashMap<>();
        options.put(ChannelProperty.CHANNEL_NAME, "Supportraum");
        options.put(ChannelProperty.CHANNEL_DESCRIPTION,
                "[size=20]Geschlossen[/size]\n" +
                        "[size=10][color=orange][b]Der Supportbot ist offline![/b][/color][/size]");
        options.put(ChannelProperty.CHANNEL_MAXCLIENTS, "0");
        options.put(ChannelProperty.CHANNEL_FLAG_MAXCLIENTS_UNLIMITED, "0");
        for (Map.Entry<Integer, Long> entry : Events.icons.entrySet()) {
            Load.getApi().deleteIcon(entry.getKey());
        }
        Utils.deleteChannel(true);
        Load.getApi().editChannel(Load.getCid(), options).awaitUninterruptibly();
        Load.log.info("Shutting down ...");
        System.exit(0);
    }

    public static void restart() {
        try {
            final String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
            final File currentJar = new File(Load.class.getProtectionDomain().getCodeSource().getLocation().toURI());


            /* is it a jar file? */
            if (!currentJar.getName().endsWith(".jar"))
                return;

            /* Build command: java -jar application.jar */
            final ArrayList<String> command = new ArrayList<String>();
            command.add(javaBin);
            command.add("-jar");
            command.add(currentJar.getPath());

            final ProcessBuilder builder = new ProcessBuilder(command);
            builder.start();
            exit();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void makePrivate(int invokerId, String pw) {
        try {
            ClientInfo ci = Load.getApi().getClientInfo(invokerId).get();
            int cid = ci.getChannelId();
            if (Load.getApi().getChannelInfo(cid).get().getTopic().equals(ci.getUniqueIdentifier())) {
                String password;
                if (pw == null) {
                    password = Utils.randomString(10);
                } else {
                    password = pw;
                }

                Map<ChannelProperty, String> prop = new HashMap<>();

                prop.put(ChannelProperty.CHANNEL_PASSWORD, password);

                Load.getApi().editChannel(cid, prop);
                Load.getApi().sendPrivateMessage(invokerId, "Passwort gesetzt: [b]" + password + "[/b]");
                Load.log.info("Passwort für Channel \"" + Load.getApi().getChannelInfo(cid).get().getName() + "\" gesetzt auf \"" + password + "\"");
            } else {
                Load.getApi().sendPrivateMessage(invokerId, "Dir gehört der Channel nicht, du darfst den Channel nicht verändern!");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
