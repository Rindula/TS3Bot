package de.rindula.tsbot.main;

import com.github.theholywaffle.teamspeak3.api.CommandFuture;
import com.github.theholywaffle.teamspeak3.api.event.*;
import com.github.theholywaffle.teamspeak3.api.exception.TS3Exception;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;

public class Events {

	public static final HashMap<Integer, Long> icons = new HashMap<>();

	public static void loadEvents() {
		Load.getApi().registerAllEvents();
		Load.getApi().addTS3Listeners(new TS3Listener() {

			@Override
			public void onTextMessage(TextMessageEvent e) {
				String[] msg = e.getMessage().split(" ");

				// Commands für Devs
				if (Load.getDeveloperUIds().containsKey(e.getInvokerUniqueId()) && msg[0].equalsIgnoreCase("!exit")) {
					Commands.exit();
					return;
				}
				try {
					// Commands für alle
					if (msg[0].equalsIgnoreCase("!private")) {
						if (msg.length == 1) {
							Commands.makePrivate(e.getInvokerId(), null);
						} else if (msg.length == 2) {
							Commands.makePrivate(e.getInvokerId(), msg[1]);
						} else {
							Load.getApi().sendPrivateMessage(e.getInvokerId(), "Benutzung: [b]!private (passwort)[/b]");
						}

						return;
					}

					// Commands für Supporter
					for (int grp :
							Load.getGroup()) {

						if (Load.getApi().getClientInfo(e.getInvokerId()).get().isInServerGroup(grp)) {
							if (msg[0].equalsIgnoreCase("!notify")) {
								Load.sendWelcomeMessage(Load.getApi().getClientInfo(e.getInvokerId()));
								return;
							}
							if (msg[0].equalsIgnoreCase("!restart")) {
								Commands.restart();
								return;
							}
						}
					}
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}

			@Override
			public void onServerEdit(ServerEditedEvent arg0) {
				// Nothing to see here
			}

			@Override
			public void onPrivilegeKeyUsed(PrivilegeKeyUsedEvent arg0) {
				// Nothing to see here
			}

			@Override
			public void onClientMoved(ClientMovedEvent e) {
				try {
					if (e.getTargetChannelId() == Load.getCid() && Load.getApi().getClientInfo(e.getClientId()).get().isRegularClient()) {
						ClientInfo cl = Load.getApi().getClientInfo(e.getClientId()).get();
						Utils.support(cl);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				Utils.deleteChannel(false);
				try {
					String cname = Load.getApi().getChannelInfo(Load.getApi().getClientInfo(e.getClientId()).get().getId()).get().getName();
					for (String c :
							Utils.getgChannels().keySet()) {
						if (c.equals(cname)) {
							System.out.println(cname);
							Utils.talk(Load.getApi().getClientInfo(e.getClientId()).get());
						}

					}
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				//Utils.updateSupport();

			}

			@Override
			public void onClientLeave(ClientLeaveEvent e) {
                if (icons.containsKey(e.getClientId())) {
					Load.getApi().deleteIcon(icons.get(e.getClientId()));
                    icons.remove(e.getClientId());
                    Load.log.info("Benutzericon entfernt");
                } else {
                    Load.log.info("Kein Icon zum entfernen");
                }
				Utils.updateSupport();
				Utils.deleteChannel(false);
			}

			/**
			 * Wenn ein Client dem Server beitritt, wird &uuml;berpr&uuml;ft ob er in einer der Supportergruppen ist. Wenn das der Fall ist, bekommt er eine Nachricht.
			 */
			@Override
			public void onClientJoin(ClientJoinEvent e) {
				try {
					for (int grp :
							Load.getGroup()) {
						if (Load.getApi().getClientByUId(e.getUniqueClientIdentifier()).get().isInServerGroup(grp)) {
							Load.sendWelcomeMessage(Load.getApi().getClientByUId(e.getUniqueClientIdentifier()));
							break;
						}
					}
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}

				Utils.updateSupport();

				/*try {
					createIcon(Load.api.getClientInfo(e.getClientId()).get());
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}*/
			}

			/**
			 * Erstellt das Developer Icon auf dem Server und f&uuml;gt es dem Client (also einem im Developer Array eingetragenen Client) hinzu
			 * @param c Die ClientInfo eines Clients
			 */
			void createIcon(ClientInfo c) {
				if (c.isServerQueryClient()) {
					return;
				}
				if (Load.getDeveloperUIds().containsKey(c.getUniqueIdentifier())) {
                    Load.log.info("Developer erkannt - setze Icon");
					try {
						URL url = Load.getDeveloperUIds().get(c.getUniqueIdentifier());
						InputStream in = new BufferedInputStream(url.openStream());
						ByteArrayOutputStream out = new ByteArrayOutputStream();
                        byte[] buf = new byte[315];
                        int n;
                        while (-1 != (n = in.read(buf))) {
                            out.write(buf, 0, n);
						}
						out.close();
                        in.close();
						byte[] response = out.toByteArray();
                        System.out.println(response.length);
						CommandFuture<Long> iconId = Load.getApi().uploadIconDirect(response);
                        iconId.onSuccess(aLong -> setPermissions(aLong, c));
                        iconId.onFailure(this::nope);
					} catch (Exception e2) {
						e2.printStackTrace();
					}
				}
			}

            private void nope(TS3Exception e) {
                e.printStackTrace();
            }

            private void setPermissions(Long aLong, ClientInfo c) {
				Load.getApi().getClientPermissions(c.getDatabaseId()).awaitUninterruptibly();
				Load.getApi().deleteClientPermission(c.getDatabaseId(), "i_icon_id");
				Load.getApi().addClientPermission(c.getDatabaseId(), "i_icon_id", aLong.intValue(), false);
                Load.log.info("Icon Berechtigung gesetzt");
                icons.put(c.getId(), aLong);
                Load.log.info("Icon Array updated");
            }

			/**
			 * Wenn ein Channel ein Passwort setzt/ändert, wird das im Log gespeichert.
			 */
			@Override
			public void onChannelPasswordChanged(ChannelPasswordChangedEvent e) {
				// Nothing to see here
			}

			@Override
			public void onChannelMoved(ChannelMovedEvent arg0) {
				// Nothing to see here
			}

			@Override
			public void onChannelEdit(ChannelEditedEvent arg0) {
				// Nothing to see here
			}

			@Override
			public void onChannelDescriptionChanged(ChannelDescriptionEditedEvent arg0) {
				// Nothing to see here
			}

			@Override
			public void onChannelDeleted(ChannelDeletedEvent arg0) {
				// Nothing to see here
			}

			@Override
			public void onChannelCreate(ChannelCreateEvent arg0) {
				// Nothing to see here
			}
		});
	}

}
