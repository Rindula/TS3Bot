package de.rindula.tsbot.main;

import com.github.theholywaffle.teamspeak3.api.ChannelProperty;
import com.github.theholywaffle.teamspeak3.api.CommandFuture;
import com.github.theholywaffle.teamspeak3.api.wrapper.Channel;
import com.github.theholywaffle.teamspeak3.api.wrapper.ChannelInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;

import java.util.*;

/**
 * Eine Klasse für Funktionen
 */
public class Utils {
    /**
     * Eine Liste mit den UIDs der Online-Supporter
     */
    private static final ArrayList<String> onlineSupps = new ArrayList<>();
    private static ArrayList<String> sChannels = new ArrayList<>();
    private static long lastSupportUpdate;
    private static Map<String, ArrayList<ChannelInfo>> gChannels = new HashMap<>();

    public static Map<String, ArrayList<ChannelInfo>> getgChannels() {
        return gChannels;
    }

    /**
     * @return Die Liste mit den UIDs der Online-Supporter
     */
    public static ArrayList<String> getOnlineSupps() {
        return onlineSupps;
    }

    /**
     * Methode zum erstellen eines Channels
     *
     * @param prop HashMap mit den Channel Einstellungen
     * @return Die ID des neues Channels, oder null wenn ein Fehler aufgetreten ist
     */
    public static int createSupportChannel(Map<ChannelProperty, String> prop) {
        try {
            CommandFuture<Integer> cid = Load.getApi().createChannel(prop.get(ChannelProperty.CHANNEL_NAME), prop);
            sChannels.add(prop.get(ChannelProperty.CHANNEL_NAME));
            return cid.get().intValue();
        } catch (InterruptedException e) {
            return Integer.parseInt(null);
        }
    }

    public static void talk(ClientInfo c) {
        int channelid;
        try {
            String cName = Load.getApi().getChannelInfo(c.getChannelId()).get().getName();
            int channelorder = getChannelOrder(Load.getApi().getChannelInfo(c.getChannelId()).get().getName());
            if (channelorder == 1) {
                channelid = Load.getApi().getChannelByNameExact(cName, true).get().getId();
            } else {
                channelid = Load.getApi().getChannelByNameExact(cName.replaceAll("\\[.*?\\] ?", "") + " #" + (channelorder - 1), true).get().getId();
            }
            Map<ChannelProperty, String> prop = new HashMap<>();
            prop.put(ChannelProperty.CHANNEL_FLAG_SEMI_PERMANENT, "1");
            prop.put(ChannelProperty.CHANNEL_TOPIC, c.getUniqueIdentifier());
            if (channelorder > 1) prop.put(ChannelProperty.CHANNEL_ORDER, String.valueOf(channelid));
            prop.put(ChannelProperty.CPID, String.valueOf(Load.getApi().getChannelByNameExact(cName, true).get().getId()));
            CommandFuture<ChannelInfo> ci = Load.getApi().getChannelInfo(Load.getApi().createChannel(cName.replaceAll("\\[.*?\\] ?", "") + " #" + channelorder, prop).get());

            if (channelorder == 1) {
                ListIterator<ChannelInfo> li = getgChannels().get(cName).listIterator(getgChannels().get(cName).size());
                while (li.hasPrevious()) {
                    ChannelInfo cinf = li.previous();
                    if (!cinf.getName().equals(cName.replaceAll("\\[.*?\\] ?", "") + " #" + channelorder)) {
                        Map<ChannelProperty, String> p = new HashMap<>();
                        p.put(ChannelProperty.CHANNEL_ORDER, "" + ci.get().getId());
                        Load.getApi().editChannel(cinf.getId(), p);
                    }
                }
            }
            Load.getApi().moveClient(c.getId(), ci.get().getId());
            Load.getApi().sendPrivateMessage(c.getId(), "Schreibe [b]!private[/b] um ein Passwort für diesen Channel zu setzen. Du kannst auch selber ein Passwort eingeben.");

            gChannels.get(cName).add(ci.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static int getChannelOrder(String name) {
        int i = 1;
        ArrayList<Integer> temp = new ArrayList<>();

        for (ChannelInfo info :
                gChannels.get(name)) {
            int current = Integer.valueOf(info.getName().split("#")[1]);
            temp.add(current);
            if (i == current) {
                i++;
                while (temp.contains(i)) i++;
            }
        }

        return i;
    }

    public static void support(ClientInfo c) {
        updateSupport();
        if (onlineSupps.size() >= 1) {
            Load.getApi().sendPrivateMessage(c.getId(), "Es wurden " + onlineSupps.size() + " Teammitglieder benachrichtigt.");
            Map<ChannelProperty, String> prop = new HashMap<>();
            String cName = "᚛ Support ᚜ » " + c.getNickname();
            prop.put(ChannelProperty.CHANNEL_MAXCLIENTS, "0");
            prop.put(ChannelProperty.CHANNEL_FLAG_MAXCLIENTS_UNLIMITED, "0");
            prop.put(ChannelProperty.CHANNEL_FLAG_SEMI_PERMANENT, "1");
            prop.put(ChannelProperty.CHANNEL_TOPIC, c.getUniqueIdentifier());
            prop.put(ChannelProperty.CHANNEL_NAME, cName);
            prop.put(ChannelProperty.CPID, "" + Load.getCid());
            CommandFuture<Channel> ch = Load.getApi().getChannelByNameExact(cName, true);
            ch.awaitUninterruptibly();
            try {
                if (ch.get() != null) {
                    CommandFuture<ChannelInfo> ci = Load.getApi().getChannelInfo(ch.get().getId());
                    Map<ChannelProperty, String> p = new HashMap<>();
                    p.put(ChannelProperty.CHANNEL_NAME, "╞ Support » " + Load.getApi().getClientByUId(ci.get().getTopic()));
                    Load.getApi().editChannel(ci.get().getId(), p);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                CommandFuture<ChannelInfo> ci = Load.getApi().getChannelInfo(createSupportChannel(prop));
                Load.getApi().addChannelPermission(ci.get().getId(), "i_channel_needed_join_power", 35).awaitUninterruptibly();
                Load.getApi().addChannelPermission(ci.get().getId(), "i_channel_needed_delete_power", 75).awaitUninterruptibly();
                Load.getApi().moveClient(c.getId(), ci.get().getId());

                for (String s :
                        onlineSupps) {
                    int cid = Load.getApi().getClientByUId(s).get().getId();
                    Load.getApi().sendPrivateMessage(cid, String.format("Es befindet sich ein neuer Supportfall im Supportchannel!\n" +
                            "[b]Userinfos:[/b]\n" +
                            "IP:\t\t\t   %s\n" +
                            "Land:\t\t   %s\n" +
                            "Plattform:\t%s\n" +
                            "Nickname:\t[URL=client://%d/%s]%s[/URL]", c.getIp(), c.getCountry(), c.getPlatform(), c.getId(), c.getUniqueIdentifier(), c.getNickname())).awaitUninterruptibly();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            Load.getApi().kickClientFromChannel(c);
            Load.getApi().pokeClient(c.getId(), "Es ist aktuell kein Supporter verfügbar.");
        }
    }

    /**
     * Updates the Support Channel depending on the amount of online Supporters
     */
    public static void updateSupport() {
        onlineSupps.clear();
        try {
            for (Client c :
                    Load.getApi().getClients().get()) {
                if (c.isServerQueryClient() || c.isAway()) continue;
                for (int s :
                        c.getServerGroups()) {
                    if (Load.getGroup().contains(s)) {
                        onlineSupps.add(c.getUniqueIdentifier());
                        break;
                    }
                }
            }
            Map<ChannelProperty, String> prop = new HashMap<>();
            if (onlineSupps.size() == 0) {
                Load.setStatus(Status.geschlossen);
                String closedString = "Supportraum [GESCHLOSSEN]";
                if (!Load.getApi().getChannelInfo(Load.getCid()).get().getName().contains(closedString)) {
                    prop.put(ChannelProperty.CHANNEL_NAME, closedString);
                    prop.put(ChannelProperty.CHANNEL_DESCRIPTION, "[size=20]Geschlossen[/size]\n[size=10][color=red]Es ist kein Teammitglied online![/color][/size]");
                    prop.put(ChannelProperty.CHANNEL_MAXCLIENTS, "1");
                    prop.put(ChannelProperty.CHANNEL_FLAG_MAXCLIENTS_UNLIMITED, "1");
                    Load.getApi().editChannel(Load.getCid(), prop);
                    prop.clear();
                }
            } else {
                Load.setStatus(Status.offen);
                String channelNameToBe = "Supportraum {" + onlineSupps.size() + " Supporter online}";
                if (!Load.getApi().getChannelInfo(Load.getCid()).get().getName().contains(channelNameToBe)) {
                    prop.put(ChannelProperty.CHANNEL_NAME, channelNameToBe);
                    prop.put(ChannelProperty.CHANNEL_DESCRIPTION, "[size=20]Support[/size]\n[size=10][color=green]Es sind aktuell " + onlineSupps.size() + " Supporter online![/color][/size]");
                    prop.put(ChannelProperty.CHANNEL_MAXCLIENTS, "1");
                    prop.put(ChannelProperty.CHANNEL_FLAG_MAXCLIENTS_UNLIMITED, "1");
                    Load.getApi().editChannel(Load.getCid(), prop);
                    prop.clear();
                }
            }
            lastSupportUpdate = System.currentTimeMillis();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param kickFrom False -> Leere Channel werden gelöscht<br>True -> Alle Channel werden geleert und gelöscht
     */
    public static void deleteChannel(boolean kickFrom) {
        Map<String, Integer> toDelete = new HashMap<>();
        for (String s :
                sChannels) {
            try {
                Channel c = Load.getApi().getChannelByNameExact(s, true).get();
                if (c.isEmpty()) {
                    Load.getApi().deleteChannel(c.getId()).awaitUninterruptibly();
                    toDelete.put(s, 0);
                } else if (kickFrom) {
                    Load.getApi().deleteChannel(c.getId()).awaitUninterruptibly();
                    toDelete.put(s, 0);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Iterator it = gChannels.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, ArrayList<ChannelInfo>> pair = (Map.Entry) it.next();

            for (ChannelInfo st :
                    pair.getValue()) {
                String s = st.getName();
                try {
                    Channel c = Load.getApi().getChannelByNameExact(s, true).get();
                    if (c.isEmpty()) {
                        Load.getApi().deleteChannel(c.getId()).awaitUninterruptibly();
                        toDelete.put(s, 1);
                    } else if (kickFrom) {
                        Load.getApi().deleteChannel(c.getId()).awaitUninterruptibly();
                        toDelete.put(s, 1);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        for (String s :
                toDelete.keySet()) {
            switch (toDelete.get(s)) {
                case 0:
                    sChannels.remove(s);
                    break;
                case 1:
                    Iterator itl = gChannels.entrySet().iterator();
                    while (itl.hasNext()) {
                        Map.Entry<String, ArrayList<ChannelInfo>> me = (Map.Entry) itl.next();
                        Iterator ita = me.getValue().iterator();
                        while (ita.hasNext()) {
                            ChannelInfo tmp = (ChannelInfo) ita.next();
                            if (tmp.getName().equals(s)) ita.remove();
                        }
                    }
                    break;
            }
        }
    }

    /**
     * @param length Die länge der Zeichenkombination
     * @return Eine zufällige Zeichenkombination aus klein- und Großbuchstaben sowie Zahlen
     */
    public static String randomString(int length) {
        String SALTCHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < length) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    public static long getLastUpdate() {
        return lastSupportUpdate;
    }
}
