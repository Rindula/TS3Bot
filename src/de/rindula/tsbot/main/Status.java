package de.rindula.tsbot.main;

/**
 * Die Status, die der Supportchannel haben kann.
 */
public enum Status {
	offen,
	geschlossen
}
